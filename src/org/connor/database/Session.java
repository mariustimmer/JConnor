package org.connor.database;

import org.connor.log.Logger;
import org.connor.database.Database;
import java.sql.PreparedStatement;

public class Session
{

	public static boolean create(int userid)
	{
		try {
			PreparedStatement statement = Database.getInstance().getConnection().prepareStatement(
				"INSERT INTO sessions (uid, created) VALUES (?, CURRENT_TIMESTAMP)"
			);
			statement.setInt(1, userid);
			statement.executeUpdate();
			return true;
		} catch (Exception exception) {
			Logger.getInstance().error(exception.getMessage());
			return false;
		}
	}

}
