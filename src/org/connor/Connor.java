package org.connor;

import org.connor.controller.AbstractController;
import org.connor.controller.RewriteController;
import org.connor.database.Database;
import org.connor.database.Session;
import org.connor.log.Logger;

public class Connor implements CommunicationReceiver
{

	public static final int MODE_DAEMON = 1;
	public static final int MODE_CONSOLE = 2;
	public static final int MODE_REWRITE = 3;
	private final int mode;
	private final Boolean verbose;
	private UserCommunicator uc;
	private static final int CONVERSATION_UUID_USERNAME = 5; 

	/**
	 * Creates a new instance of Connor.
	 * @param mode Mode to use Connor with
	 * @param verbose True if verbose mode is enabled or false
	 */
	public Connor(int mode, Boolean verbose)
	{
		this.mode = mode;
		this.verbose = verbose;
	}

	/**
	 * Will be called when there is a response coming from a communicator.
	 * @param message Incomming message as string
	 * @param uuid Identifier for the conversation
	 */
	public void onReceive(String message, int uuid)
	{
		if (uuid != 0) {
			switch (uuid) {
				default:
					this.uc.print("Unfortunately I forgot my question");
			}
		}
	}

	/**
	 * Determinates the mode Conner is currently running with.
	 * @return MODE_DAEMON or MODE_CONSOLE
	 */
	public int getMode()
	{
		return this.mode;
	}

	/**
	 * Determinates whether verbose mode is enabled or not.
	 * @return True if verbose mode or false
	 */
	public Boolean isVerbose()
	{
		return this.verbose;
	}

	public void boot() throws Exception
	{
		if (Database.getInstance().getConnection() == null) {
                        /**
                         * There is no database connection available which
                         * makes everything else senseless so we can throw
                         * an exception here.
                         */
                        throw new Exception("No database connection available");
                }
		Logger.getInstance().debug("Database connection available");
		Session.create(1);
		AbstractController controller = null;
		switch (this.getMode()) {
			case Connor.MODE_REWRITE:
				controller = new RewriteController(this);
				break;
		}
		if (controller != null) {
			/**
			 * Correct controller for the requested
			 * mode created and ready to be used.
			 */
			(new Thread(controller, this.getClass().getSimpleName())).start();
		} else {
			/**
			 * There was no controller mapped to be executed so
			 * we just assume that the requested mode is not
			 * implemented yet.
			 * This will be written to the log and JConner exits.
			 */
            String message = "Mode not implemented yet";
			Logger.getInstance().error(message);
            System.err.println(message);
		}
	}

}
