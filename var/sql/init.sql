-- init.sql
-- 2018-08-10
-- Marius Timmer
-- Initialize the database by creating the required tables and relations and
-- filling them with initial data in order to run the first execution of JConnor.

DROP TABLE IF EXISTS sessions;
DROP TABLE IF EXISTS persons;
DROP TABLE IF EXISTS users_roles;
DROP TABLE IF EXISTS roles;
DROP TABLE IF EXISTS users;

CREATE TABLE users (
	uid SERIAL,
	username VARCHAR(32) NOT NULL,
	created TIMESTAMP NOT NULL,
	CONSTRAINT pk_users PRIMARY KEY (uid),
	CONSTRAINT uc_users_username UNIQUE (username)
);
CREATE TABLE roles (
	rid SERIAL,
	name VARCHAR(32),
	created TIMESTAMP NOT NULL,
	CONSTRAINT pk_roles PRIMARY KEY (rid),
	CONSTRAINT uc_roles_name UNIQUE (name)
);
CREATE TABLE users_roles (
	uid INTEGER,
	rid INTEGER,
	created TIMESTAMP NOT NULL,
	CONSTRAINT pk_users_roles PRIMARY KEY (uid, rid),
	CONSTRAINT fk_users_roles_users FOREIGN KEY (uid) REFERENCES users(uid),
	CONSTRAINT fk_users_roles_roles FOREIGN KEY (uid) REFERENCES roles(rid)
);
CREATE TABLE persons (
	pid SERIAL,
	first_name VARCHAR(64) DEFAULT NULL,
	last_name VARCHAR(64) DEFAULT NULL,
	gender INTEGER DEFAULT 0,
	day_of_birth DATE DEFAULT NULL,
	created TIMESTAMP NOT NULL,
	CONSTRAINT pk_persons PRIMARY KEY (pid),
	CONSTRAINT uc_persons UNIQUE (first_name, last_name)
);
CREATE TABLE sessions (
	sis SERIAL,
	uid INTEGER,
	created TIMESTAMP NOT NULL,
	CONSTRAINT pk_sessions PRIMARY KEY (sis),
	CONSTRAINT fk_sessions_users FOREIGN KEY (uid) REFERENCES users(uid)
);

INSERT INTO users (
	uid,
	username,
	created
) VALUES
	(1, 'connor', CURRENT_TIMESTAMP),
	(2, 'root', CURRENT_TIMESTAMP)
;

INSERT INTO roles (
	rid,
	name,
	created
) VALUES
	(1, 'system', CURRENT_TIMESTAMP),
	(2, 'administrator', CURRENT_TIMESTAMP),
	(3, 'regular user', CURRENT_TIMESTAMP)
;

INSERT INTO users_roles (
	uid,
	rid,
	created
) VALUES
	(1, 1, CURRENT_TIMESTAMP),
	(1, 2, CURRENT_TIMESTAMP),
	(2, 2, CURRENT_TIMESTAMP)
;

INSERT INTO persons (
	first_name,
	last_name,
	gender,
	day_of_birth,
	created
) VALUES
	('Connor', NULL, 1, '2018-08-10', CURRENT_TIMESTAMP)
;
