package org.connor;

import org.connor.log.Logger;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import org.json.JSONException;
import org.json.JSONObject;

public class Configurator
{

	public static final String CONFIGURATION_FILENAME = "connor.json";
	private static Configurator configurator;
	private JSONObject data;

	private Configurator()
	{
		this.load();
	}

	public static Configurator getInstance()
	{
		if (Configurator.configurator == null) {
			Configurator.configurator = new Configurator();
		}
		return Configurator.configurator;
	}

	private void load()
	{
		String jsondata = new String();
		try {
			FileReader reader = new FileReader(Configurator.CONFIGURATION_FILENAME);
			BufferedReader br = new BufferedReader(reader);
			String line = null;
			while ((line = br.readLine()) != null) {
				jsondata += line;
			}
		} catch (IOException exception) {
			Logger.getInstance().error(exception.getMessage());
		}
		try {
			this.data = new JSONObject(jsondata);
		} catch (JSONException exception) {
			Logger.getInstance().error(exception.getMessage());
			this.data = new JSONObject();
		}
	}

	public String get(String key)
	{
		return (this.data.has(key)) ? this.data.getString(key) : new String();
	}

}
