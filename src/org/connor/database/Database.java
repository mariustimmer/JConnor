package org.connor.database;

import org.connor.Configurator;
import org.connor.log.Logger;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;

public class Database
{

	private static Database database;
	private Connection connection;

	private Database()
	{
		this.connect();
	}

	private Boolean connect()
	{
		try {
			Logger.getInstance().debug("Connecting to database");
			Class.forName("org.postgresql.Driver");
			Logger.getInstance().debug("PostgreSQL driver loaded");
			//String dsn = Configurator.getInstance().get("dbdsn");
			//this.connection = DriverManager.getConnection(dsn, "jconnor");
			String url = String.format(
				"jdbc:postgresql://%s:%s/%s",
				Configurator.getInstance().get("dbhost"),
				Configurator.getInstance().get("dbport"),
				Configurator.getInstance().get("dbname")
			);
			Logger.getInstance().debug("Build connection string");
			Properties props = new Properties();
			props.setProperty("user", Configurator.getInstance().get("dbuser"));
			props.setProperty("ssl", Configurator.getInstance().get("dbssl"));
			props.setProperty("sslmode", Configurator.getInstance().get("dbsslmode"));
			props.setProperty("sslcert", Configurator.getInstance().get("dbsslcert"));
			props.setProperty("sslkey", Configurator.getInstance().get("dbsslkey"));
			props.setProperty("loggerLevel", "DEBUG");
			props.setProperty("loggerFile", "./var/postgres.log");
			Logger.getInstance().debug("Connection properties are set up");
			this.connection = DriverManager.getConnection(url, props);
			Logger.getInstance().debug("Connection established");
			return true;
		} catch (Exception exception) {
			Logger.getInstance().error(exception.getMessage());
			return false;
		}
	}

	public Connection getConnection()
	{
		return this.connection;
	}

	public static Database getInstance()
	{
		if (Database.database == null) {
			Database.database = new Database();
		}
		return Database.database;
	}

}

