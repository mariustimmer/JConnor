package org.connor.controller;

import org.connor.CommunicationReceiver;
import org.connor.Connor;
import org.connor.UserCommunicator;
import java.lang.Runnable;

/**
 * AbstractController
 * This abstract class is meant to be the parent of all controllers executed
 * during the boot sequence depending on the requested mode.
 */
abstract public class AbstractController extends UserCommunicator implements Runnable, CommunicationReceiver
{

	private Connor connor;

	protected AbstractController()
	{
		this(null);
	}

	/**
	 * Generates a new controller.
	 * @param connor Executing Connor instance
	 */
	public AbstractController(Connor connor)
	{
		super(connor);
		this.connor = connor;
	}

	/**
	 * Returns the connor object which executes this controller.
	 * @return Connor
	 */
	protected Connor getConnor()
	{
		return this.connor;
	}

	/**
	 * This method is required in order to work as a communicator.
	 * This method will do nothing and should be overwritten
	 * by other classes.
	 */
	public void onReceive(String data, int uuid)
	{

	}

}
