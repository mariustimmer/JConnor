package org.connor.database;

/**
 * Abstract DAO class
 * This class is meant to be the parent of all DAO classes used by connor.
 */
abstract public class DAO
{

	private String table_name;
	private String[] columns;

	/**
	 * Generates the DAO class for the requested entity.
	 * @param table_name Name of the database table
	 * @param columns Name of the columns
	 */
	protected DAO(String table_name, String[] columns)
	{
		this.table_name = table_name;
		this.columns = columns;
	}

	/**
	 * Returns the table name used for this DAO class.
	 * @return Table name
	 */
	public String getTableName()
	{
		return this.table_name;
	}

	/**
	 * Returns all the known column names.
	 * @return Column names
	 */
	public String[] getColumnNames()
	{
		return this.columns;
	}

}
