package org.connor;

interface ICommunicator
{

	/**
	 * Returns the current receiver.
	 * @return CommunicationReceiver
	 */
	public CommunicationReceiver getReceiver();

	/**
	 * Sends a string to the communication partner.
	 * Use the uuid to separate multiple incomming responses later.
	 * @param message Message to send
	 * @param uuid UUID to separate messages and responses
	 * @return True on success or false
	 */
	public boolean send(String message, int uuid);

}
