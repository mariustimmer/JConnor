package org.connor.controller;

import org.connor.Connor;
import org.connor.database.Database;
import org.connor.log.Logger;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.InterruptedException;
import java.lang.Process;
import java.lang.Runtime;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class RewriteController extends AbstractController
{

	private static final int UUID_RECOMPILE = 1;

	public RewriteController(Connor connor)
	{
		super(connor);
	}

	public void run()
	{
		this.setCommunicationReceiver(this);
		this.print("Rewriting myself...");
		try {
			this.generateDatabaseClasses();
			this.send(
				"Do you want me to compile myself?",
				UUID_RECOMPILE
			);
		} catch (Exception exception) {
			this.print(
				String.format(
					"Could not rewrite my code: %s",
					exception.getMessage()
				)
			);
		}
	}

	public static String convertType(final String raw_type)
	{
		switch (raw_type) {
			case "integer":
				return "int";
			case "timestamp without time zone":
			case "date":
				return "long";
			case "character varying":
			default:
				return "String";
		}
	}

	private static String upperCaseConstant(String rawname)
	{
		return rawname.toUpperCase();
	}

	private static String lowCamelCase(String rawname)
	{
		String newname = RewriteController.camelcase(rawname);
		return newname.substring(0, 1).toLowerCase() + newname.substring(1);
	}

	private static String camelcase(String rawname)
	{
		String newname = rawname;
		if (rawname.indexOf('_') != -1) {
			int position = rawname.indexOf('_');
			newname = rawname.substring(
				0,
				position
			).toLowerCase() + rawname.substring(
				position + 1,
				position + 2
			).toUpperCase();
			if (rawname.length() > position + 1) {
				newname += rawname.substring(
					position + 2
				).toLowerCase();
			}
			newname = RewriteController.camelcase(newname);
		}
		return newname.substring(0, 1).toUpperCase() + newname.substring(1);
	}

	private static boolean save(String filename, String content)
	{
		try {
			File file = new File(filename);
			BufferedWriter bw = new BufferedWriter(
				new FileWriter(
					file
				)
			);
			bw.write(content);
			bw.close();
			return true;
		} catch (IOException exception) {
			Logger.getInstance().error(exception.getMessage());
			return false;
		}
	}

	private ArrayList<String> fetchTableNames()
	{
		ArrayList<String> table_names = new ArrayList<>();
		try {
			Statement statement = Database.getInstance().getConnection().createStatement();
			ResultSet resultset = statement.executeQuery(
				String.format(
					"SELECT table_name FROM information_schema.tables WHERE table_schema = 'public'"
				)
			);
			while (resultset.next()) {
				table_names.add(resultset.getString("table_name"));
			}
		} catch (Exception exception) {
			/**
			 * Could not read any table names from the database
			 * since an error occured. We will stop here.
			 */
			Logger.getInstance().error(exception.getMessage());
		}
		return table_names;
	}

	private HashMap<String, String> fetchColumns(String table_name)
	{
		HashMap<String, String> columns = new HashMap<>();
		try {
			Statement statement = Database.getInstance().getConnection().createStatement();
			ResultSet resultset = statement.executeQuery(
				String.format(
					"SELECT column_name, data_type FROM information_schema.columns WHERE table_schema = 'public' AND table_name = '%s'",
					table_name
				)
			);
			while (resultset.next()) {
				columns.put(
					resultset.getString("column_name"),
					resultset.getString("data_type")
				);
			}
		} catch (Exception exception) {
			Logger.getInstance().error(exception.getMessage());
		}
		return columns;
	}

	private void generateDatabaseClasses() throws Exception
	{
		ArrayList<String> table_names = this.fetchTableNames();
		this.print("Generating database classes");
		for (String table_name: table_names) {
			File dto_dir = new File("./src/org/connor/database/dto");
			File dao_dir = new File("./src/org/connor/database/dao");
			dto_dir.mkdirs();
			dao_dir.mkdirs();
			if ((!dto_dir.exists()) ||
			    (!dao_dir.exists())) {
				throw new Exception(
					"Could not create dao or dto class directory!"
				);
			}
			this.print("  "+ table_name);
			HashMap<String, String> column_names = this.fetchColumns(table_name);
			String dto_filename = String.format(
				"./src/org/connor/database/dto/%s.java",
				RewriteController.camelcase(table_name)
			);
			this.print("    DTO -> "+ dto_filename);
			String dto_class = this.generateDTO(table_name, column_names);
			boolean success = RewriteController.save(
				dto_filename,
				dto_class
			);
			if (success) {
				Logger.getInstance().debug(
					String.format(
						"DTO class file written to \"%s\"",
						dto_filename
					)
				);
			} else {
				throw new Exception(
					String.format(
						"Could not write DTO class file \"%s\"",
						dto_filename
					)
				);
			}
			String dao_filename = String.format(
				"./src/org/connor/database/dao/%sDAO.java",
				RewriteController.camelcase(table_name)
			);
			this.print("    DAO -> "+ dao_filename);
			String dao_class = this.generateDAO(table_name, column_names);
			success = RewriteController.save(
				dao_filename,
				dao_class
			);
			if (success) {
				Logger.getInstance().debug(
					String.format(
						"DAO class file written to \"%s\"",
						dao_filename
					)
				);
			} else {
				throw new Exception(
					String.format(
						"Could not write DAO class file \"%s\"",
						dao_filename
					)
				);
			}
		}
		this.print("Finished the job");
	}

	private String generateDAO(String table_name, HashMap<String, String> column_names)
	{
		StringBuilder output    = new StringBuilder();
		StringBuilder methods   = new StringBuilder();
		StringBuilder constants = new StringBuilder();
		int i = 0;
		output.append("package org.connor.database.dao;\n\nimport org.connor.database.DAO;\n\npublic class ");
		output.append(RewriteController.camelcase(table_name));
		output.append("DAO extends DAO {\n\n");
		constants.append(
			String.format(
				"\tpublic static final String TABLE_NAME = \"%s\";\n",
				table_name
			)
		);
		methods.append(
			String.format(
				"\tpublic %sDAO()\n\t{\n\t\tsuper(\n\t\t\tTABLE_NAME,\n\t\t\tnew String[]{\n",
				RewriteController.camelcase(table_name)
			)
		);
		Iterator iterator = column_names.entrySet().iterator();
		while (iterator.hasNext()) {
			i++;
			Map.Entry me = (Map.Entry)iterator.next();
			String name          = me.getKey().toString();
			String database_type = column_names.get(name);
			String java_type     = RewriteController.convertType(database_type);
			String constant_name = "COLUMN_"+ RewriteController.upperCaseConstant(name);
			constants.append("\tpublic static final String ");
			constants.append(constant_name);
			constants.append(" = \"");
			constants.append(name);
			constants.append("\";\n");
			methods.append("\t\t\t\t");
			methods.append(constant_name);
			if (i < column_names.size()) {
				methods.append(",");
			}
			methods.append("\n");
		}
		methods.append("\t\t\t}\n\t\t);\n\t}");
		output.append(constants);
		output.append("\n");
		output.append(methods);
		output.append("\n\n}\n");
		return output.toString();
	}

	private String generateDTO(String table_name, HashMap<String, String> column_names)
	{
		StringBuilder output = new StringBuilder();
		output.append("package org.connor.database.dto;\n\nimport org.connor.database.AbstractDTO;\n\npublic class ");
		output.append(RewriteController.camelcase(table_name));
		output.append(" extends AbstractDTO {\n\n");
		Iterator iterator = column_names.entrySet().iterator();
		while (iterator.hasNext()) {
			Map.Entry me = (Map.Entry)iterator.next();
			String name          = me.getKey().toString();
			String database_type = column_names.get(name);
			String java_type     = RewriteController.convertType(database_type);
			output.append("\tprivate ");
			output.append(java_type);
			output.append(" ");
			output.append(RewriteController.lowCamelCase(name));
			output.append(";\n\n\tpublic ");
			output.append(java_type);
			output.append(" get");
			output.append(RewriteController.camelcase(name));
			output.append("()\n\t{\n\t\treturn this.");
			output.append(RewriteController.lowCamelCase(name));
			output.append(";\n\t}\n\n\tpublic void set");
			output.append(RewriteController.camelcase(name));
			output.append("(");
			output.append(java_type);
			output.append(" ");
			output.append(RewriteController.lowCamelCase(name));
			output.append(")\n\t{\n\t\tthis.");
			output.append(RewriteController.lowCamelCase(name));
			output.append(" = ");
			output.append(RewriteController.lowCamelCase(name));
			output.append(";\n\t}\n\n");
		}
		output.append("}\n");
		return output.toString();
	}

	public void recompile()
	{
		try {
			Runtime runtime = Runtime.getRuntime();
			Process process = runtime.exec("ant dist");
			process.waitFor();
			if (process.exitValue() == 0) {
				this.print("I have compiled myself successfully!");
			} else {
				this.print("I wasn't even able to clean up this mess");
			}
		} catch (IOException exception) {
			this.print("I couldn't execute the recompilation");
			Logger.getInstance().error(exception.getMessage());
		} catch (InterruptedException exception) {
			this.print("Someone -or something- interrupted me");
			Logger.getInstance().error(exception.getMessage());
		}
	}

	public void onReceive(String data, int uuid)
	{
		switch (uuid) {
			case UUID_RECOMPILE:
				if ((data.length() > 1) &&
				    (data.substring(0, 1).equalsIgnoreCase("Y"))) {
					this.print("Thanks");
					this.recompile();
				} else {
					this.print("Then you will have to compile me by hand later");
				}
				break;
			default:
				this.print("I have no idea what you want");
		}
	}

}
