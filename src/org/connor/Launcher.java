package org.connor;

import org.connor.log.Logger;

import gnu.getopt.Getopt;
import gnu.getopt.LongOpt;

class Launcher implements Runnable
{

	private Integer mode;
	private Boolean verbose;
	private String[] arguments;

	/**
	 * Creates a new Launcher instance which should only be accessed by
	 * the main method within this class since it will be started as a
	 * Runnable for a new thread.
	 * @param mode Requested mode to use
	 * @param verbose True if this should be verbose or false
	 * @param args Arguments given from the command line
	 */
	private Launcher(Integer mode, Boolean verbose, String[] args)
	{
		this.mode = mode;
		this.verbose = verbose;
		this.arguments = args;
	}

	/**
	 * Returns wheather the current cycle is verbose or not.
	 * @return True if verbose or false
	 */
	public Boolean isVerbose()
	{
		return this.verbose;
	}

	/**
	 * Returns the current mode.
	 * @return Connor.MODE_* constants
	 */
	public Integer getMode()
	{
		return this.mode;
	}

	/**
	 * Returns all the arguments given from the command line.
	 * @return List of all arguments
	 */
	protected String[] getArguments()
	{
		return this.arguments;
	}

	/**
	 * This is where the operating system starts Connor.
	 * @param args Arguments given from the command line
	 */
	public static void main(String[] args)
	{
		Logger.getInstance().debug("Connor started");
		LongOpt[] longopts = new LongOpt[4];
		longopts[0] = new LongOpt("daemon", LongOpt.NO_ARGUMENT, null, 'd');
		longopts[1] = new LongOpt("console", LongOpt.NO_ARGUMENT, null, 'c');
		longopts[2] = new LongOpt("verbose", LongOpt.NO_ARGUMENT, null, 'v');
		longopts[3] = new LongOpt("rewrite", LongOpt.NO_ARGUMENT, null, 'r');
		Getopt g = new Getopt("Connor", args, "", longopts);
		Integer mode = Connor.MODE_CONSOLE;
		Boolean verbose = false;
		int c = 0;
		while ((c = g.getopt()) != -1) {
			Logger.getInstance().debug(
				String.format(
					"Got argument %c",
					c
				)
			);
			switch (c) {
				case 'd':
					Logger.getInstance().debug("Booting in daemon mode");
					mode = Connor.MODE_DAEMON;
					break;
				case 'c':
					Logger.getInstance().debug("Booting in interactive console mode");
					mode = Connor.MODE_CONSOLE;
					break;
				case 'v':
					Logger.getInstance().debug("Using verbose mode");
					verbose = true;
					break;
				case 'r':
					Logger.getInstance().debug("Rewriting the code");
					mode = Connor.MODE_REWRITE;
					break;
			}
		}
		/**
		 * Now everything should be prepared so we can
		 * create a new instance in a whole new thread.
		 */
		try {
			(
				new Thread(
					new Launcher(
						mode,
						verbose,
						args
					)
				)
			).start();
		} catch (Exception exception) {
			Logger.getInstance().error(
				String.format(
					"Failed to launch new thread: %s",
					exception.getMessage()
				)
			);
		}
	}

	/**
	 * Here a new instance of Connor will be created in its own thread
	 * so it is independent from the original (user) thread.
	 */
	public void run()
	{
		try {
			Logger.getInstance().debug(
				String.format(
					"New thread started as user \"%s\"",
					Configurator.getInstance().get("username")
				)
			);
			Connor connor = new Connor(
				this.getMode(),
				this.isVerbose()
			);
			connor.boot();
			Logger.getInstance().debug("Connor exited");
		} catch (Exception exception) {
			/**
			 * Lets catch all kinds of possible exceptions thrown
			 * by the Connor object. This will prevent the output
			 * to be filled with random java errors and provide
			 * a clean way for fatal errors.
			 */
			Logger.getInstance().error(
				String.format(
					"Fatal connor error: %s",
					exception.getMessage()
				)
			);
		}
	}

}
