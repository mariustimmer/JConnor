package org.connor.log;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Logger {

	public static final String DEFAULT_FILENAME = "./connor.log";
	private static Logger static_logger;
	private String filename;
	private LogLevel loglevel;

	public Logger()
	{
		this(Logger.DEFAULT_FILENAME);
	}

	public Logger(String filename)
	{
		this(filename, LogLevel.NOTICE);
	}

	public Logger(String filename, LogLevel loglevel)
	{
		this.filename = filename;
		this.loglevel = loglevel;
	}

	public static Logger getInstance()
	{
		if (Logger.static_logger == null) {
			Logger.static_logger = new Logger();
		}
		return Logger.static_logger;
	}

	public String getFilename()
	{
		return this.filename;
	}

	public LogLevel getLoglevel()
	{
		return this.loglevel;
	}

	protected boolean write(String message, LogLevel level)
	{
		String timestring = new String();
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
			timestring = sdf.format(new Date());
		} catch (IllegalArgumentException exception) {
			/**
			 * Something went wrong formatting the current time stamp as
			 * human readable. We will use an empty string for the time
			 * which will be seen in the log. We shoudln't hide maybe
			 * important log messages because of this. 
			 */
			timestring = "No timestamp";
		}
		String line = String.format(
			"%s  [%9s]  %s",
			timestring,
			loglevel.name(),
			message
		);
		BufferedWriter writer = null;
		try {
			FileWriter filewriter = new FileWriter(this.filename, true);
			writer = new BufferedWriter(filewriter);
			writer.write(line);
			writer.newLine();
			writer.close();
			return true;
		} catch (IOException exception) {
			if (writer != null) {
				try {
					writer.close();
				} catch (IOException inner_exception) {
					/**
					 * During the exception another exception occurred while
					 * closing the file pointer. We can assume that the file
					 * pointer was just not open.
					 */
				}
			}
			/**
			 * Could not write to the log file. In this case we can not write
			 * a error message to the log file. So lets just return false and
			 * hope our calling method will recognize it.
			 */
			return false;
		}
	}

	public boolean debug(String message)
	{
		return this.write(message, LogLevel.DEBUG);
	}

	public boolean info(String message)
	{
		return this.write(message, LogLevel.INFO);
	}

	public boolean notice(String message)
	{
		return this.write(message, LogLevel.NOTICE);
	}

	public boolean warning(String message)
	{
		return this.write(message, LogLevel.WARNING);
	}

	public boolean error(String message)
	{
		return this.write(message, LogLevel.ERROR);
	}

	public boolean emergency(String message)
	{
		return this.write(message, LogLevel.EMERGENCY);
	}

}
