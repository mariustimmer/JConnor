package org.connor.database;

import org.connor.log.Logger;
import java.lang.Class;
import java.lang.ClassNotFoundException;

/**
 * Abstract DTO class
 * This class is meant to be the parent of all DTO classes used by connor.
 */
abstract public class AbstractDTO
{

}
