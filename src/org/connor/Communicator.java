package org.connor;

import java.util.logging.Logger;

abstract class Communicator implements ICommunicator
{

	private CommunicationReceiver receiver;

	public Communicator(CommunicationReceiver receiver)
	{
		this.setCommunicationReceiver(receiver);
	}

	protected void setCommunicationReceiver(CommunicationReceiver receiver)
	{
		this.receiver = receiver;
	}

	public CommunicationReceiver getReceiver()
	{
		return this.receiver;
	}

	protected void gotResponse(String message, int uuid)
	{
		if (this.getReceiver() != null) {
			this.getReceiver().onReceive(
				message,
				uuid
			);
		} else {
			/**
			 * There is no receiver set which can be used. This is
			 * an mistake made by the developer. Just print out a
			 * warning to inform the developer or tester during
			 * development. This should never go out to a end user.
			 */
			Logger.getLogger(
				Communicator.class.getName()
			).warning(
				String.format(
					"There is no message receiver set for %s!",
					this.getClass().getSimpleName()
				)
			);
		}
	}

}
