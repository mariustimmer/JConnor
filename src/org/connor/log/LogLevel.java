package org.connor.log;

public enum LogLevel {

	DEBUG,
	INFO,
	NOTICE,
	WARNING,
	ERROR,
	EMERGENCY;

}
