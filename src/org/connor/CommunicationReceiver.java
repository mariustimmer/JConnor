package org.connor;

/**
 * Communication Receiver
 * This communication receiver will be used by the communicator
 * to provide the data sent by the partner. Implement this interface
 * so you can receive messages from communicators.
 * @author Marius Timmer
 * @version 0.1
 */
public interface CommunicationReceiver
{

	/**
	 * Will be called after the communication partner has sent the message.
	 * @param data Incomming message as string
	 * @param uuid ID to separate multiple incomming messages
	 */
	public void onReceive(String data, int uuid);

}
