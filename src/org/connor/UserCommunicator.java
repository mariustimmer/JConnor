package org.connor;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class UserCommunicator extends Communicator
{

	public static final int UUID_NO_RESPONSE = 0;
	public static final int UUID_CUSTOM = -1;

	public UserCommunicator(CommunicationReceiver receiver)
	{
		super(receiver);
	}

	public boolean send(String message, int uuid)
	{
		// First we print out the message to send
		System.out.println(
			String.format(
				"> %s",
				message
			)
		);
		if ((this.getReceiver() == null) ||
			(uuid == UUID_NO_RESPONSE)) {
			return true;
		}
		// Then we read in from stdin
		String response = null;
		BufferedReader br = null;
		try {
			br = new BufferedReader(
				new InputStreamReader(
					System.in
				)
			);
			response = br.readLine();
			br.close();
		} catch (IOException outerexception) {
			if (br != null) {
				try {
					br.close();
				} catch (IOException innerexception) {
					return false;
				}
			}
			return false;
		}
		br = null;
		this.gotResponse(response, uuid);
		return true;
	}

	public void print(String message)
	{
		this.send(message, UUID_NO_RESPONSE);
	}

}
